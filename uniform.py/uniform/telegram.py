from dataclasses import dataclass
from string import Template
from typing import Dict, List

from aiosmtplib import SMTP, SMTPRecipientsRefused, SMTPResponseException
from httpx import AsyncClient

from .datastructures import Field


@dataclass(frozen=True)
class TelegramBotConfig:
    bot_token: str
    chat_id: str
    fields: List[Field]
    message_template: Template


TG_BOT_CONFIGS: Dict[str, TelegramBotConfig] = {}
TG_BOT_CLIENTS: Dict[str, AsyncClient] = {}


def tg_bot_client(bot_token: str) -> AsyncClient:
    if bot_token not in TG_BOT_CLIENTS:
        TG_BOT_CLIENTS[bot_token] = AsyncClient(
            headers={"Content-Type": "application/x-www-form-urlencoded"},
            base_url=f"https://api.telegram.org/bot{bot_token}",
            timeout=1,
        )

    return TG_BOT_CLIENTS[bot_token]


async def send_telegram_message(
    client: AsyncClient, chat_id: str, message: str
) -> bool:
    params = {"chat_id": chat_id, "text": message}
    async with client:
        response = await client.get("/sendMessage", params=params)

    # TODO: fix this issue
    return response.json()["ok"]  # type: ignore
