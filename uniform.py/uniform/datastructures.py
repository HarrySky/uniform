from dataclasses import dataclass
from typing import List, Optional, Union


@dataclass(frozen=True)
class FieldLimit:
    type: str
    value: Union[int, float, str]


@dataclass(frozen=True)
class Field:
    name: str
    limits: List[FieldLimit]
    is_number: bool
    default: Optional[Union[float, str]] = None
