from typing import Union

from starlette import status
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import JSONResponse, PlainTextResponse

from .parsing import parse_form
from .smtp import send_smtp_message, smtp_client
from .telegram import TG_BOT_CONFIGS, send_telegram_message, tg_bot_client

UNIFORM = Starlette()


# Accepts application/x-www-form-urlencoded
# Returns JSON with status and details
@UNIFORM.route("/tgbot/{name}", methods=["POST"])
async def telegram_bot_endpoint(
    request: Request
) -> Union[PlainTextResponse, JSONResponse]:
    name = request.path_params.get("name", None)
    if name is None:
        return PlainTextResponse(
            "Config Name Missing", status_code=status.HTTP_400_BAD_REQUEST
        )

    config = TG_BOT_CONFIGS.get(name, None)
    if config is None:
        return PlainTextResponse(
            f"Config '{name}' Not Found", status_code=status.HTTP_404_NOT_FOUND
        )

    fields, errors = await parse_form(request, config.fields)
    if errors:
        return JSONResponse(errors, status_code=status.HTTP_400_BAD_REQUEST)

    # MyPy warns about signature, but it is wrong
    message = config.message_template.substitute(fields)  # type: ignore
    client = tg_bot_client(config.bot_token)
    if not await send_telegram_message(client, config.chat_id, message):
        return PlainTextResponse(
            "Telegram Side Problem", status_code=status.HTTP_502_BAD_GATEWAY
        )

    return PlainTextResponse("Success", status_code=status.HTTP_200_OK)


# Accepts application/x-www-form-urlencoded
# Returns JSON with status and details
@UNIFORM.route("/smtp/{name}", methods=["POST"])
async def smtp_endpoint(request: Request) -> PlainTextResponse:
    # TODO: finish
    name = request.path_params.get("name", None)
    if not name:
        return PlainTextResponse(
            "SMTP Config Name Missing", status_code=status.HTTP_400_BAD_REQUEST
        )

    return PlainTextResponse(name)
