module.exports = {
  verbose: true,
  browser: true,
  modulePathIgnorePatterns: [
    "<rootDir>/build/",
    "<rootDir>/src/__tests__/utils.ts"
  ],
  collectCoverageFrom: [
    "src/*.ts"
  ],
  collectCoverage: true,
};