import axios, { AxiosResponse } from 'axios';

/**
 * Interface for Object with values of string, number and/or boolean types.
 */
export interface FormData {
  [key: string]: string | number | boolean;
}

/**
 * Encoding of booleans in encodeURIComponent is not what I want.
 * This method wraps encodeURIComponent and encodes booleans
 * as 0 (false) or 1 (true). Other types are untouched.
 * @param value any value of string, number or boolean type to encode.
 */
export function encodeValue(value: string | number | boolean): string {
  if (typeof value === 'boolean') {
    value = value ? 1 : 0;
  }

  return encodeURIComponent(value);
}

/**
 * Converts FormData object to query parameters string.
 *
 * Example:
 *
 * Object
 * - { k1: 42, k2: "abc", k3: true }
 *
 * Will be converted to
 * - "k1=42&k2=abc&k3=1"
 * @param formData FormData object to convert.
 */
export function formDataToString(formData: FormData): string {
  return Object.keys(formData)
    .map(key => {
      const value = formData[key];
      return `${encodeURIComponent(key)}=${encodeValue(value)}`;
    })
    .join('&');
}

/**
 * Sends POST request with FormData encoded as 'application/x-www-form-urlencoded'.
 * @param formData FormData object to convert and send.
 * @param backendUrl URL to send POST request to.
 */
export function sendFormData(
  formData: FormData,
  backendUrl: string
): Promise<AxiosResponse> {
  const formBody = formDataToString(formData);
  return axios.post(backendUrl, formBody, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });
}
